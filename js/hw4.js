

function theme(){
        function storage1(){
            localStorage.setItem('bgcolor', 'white');
            localStorage.setItem('p', 'black');
            localStorage.setItem('list', 'white');
            localStorage.setItem('pd1', '#35444F');
            localStorage.setItem('pd2', 'rgba(99, 105, 110, 0.48)');
            window.location.reload();
        }
        function storage2(){
            localStorage.setItem('bgcolor', 'black');
            localStorage.setItem('list', 'orange');
            localStorage.setItem('pd1', 'red');
            localStorage.setItem('pd2', 'red');
            localStorage.setItem('p', 'orange');
            window.location.reload();
        }
        document.querySelector('#btn1').onclick = function(){
            storage2();
        }

        document.querySelector('#btn2').onclick = function(){
            storage1();
        }        
            
        document.querySelector('body').style.backgroundColor = localStorage.getItem('bgcolor');
        document.querySelectorAll('a').forEach(item => item.style.color = localStorage.getItem('list'));
        document.querySelector('img').style.backgroundColor = localStorage.getItem('list');
        document.querySelectorAll('.menu > li > a').forEach(item => item.style.color = localStorage.getItem('pd1'));
        document.querySelector('.list').style.backgroundColor = localStorage.getItem('pd1');
        document.querySelector('footer').style.backgroundColor = localStorage.getItem('pd2');
        document.querySelectorAll('p, .footer-menu > li > a').forEach(el => el.style.color = localStorage.getItem('p'));
        document.querySelector('header > a').style.color = localStorage.getItem('p');


    }
    theme();

